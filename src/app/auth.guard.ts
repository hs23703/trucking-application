import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map} from 'rxjs/operators';
import { LoginService } from './modules/login/login.service';
import { CommonService } from './services/common.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(private commonService : CommonService, private loginService : LoginService, private router: Router){

  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.isLoggedin();
  }
  isLoggedin() {
    if(this.commonService.isLoggedIn)
    return true;

    else if (window.localStorage.getItem('login')){
      return this.login();
    }

    else
    this.router.navigate(['/login']);
    return false
  }
  login() {
    const sessionObj =  JSON.parse(window.localStorage.getItem('login') || '');
    console.log('=================session obj ============',sessionObj);
   return this.loginService.loginViaUserToken(sessionObj["user-token"]).pipe(
      map( (response : any)  =>{
        if(response == false)
        this.router.navigate(['/login']);
        
        return response;
      }),
      catchError((err) => {
        this.router.navigate(['/login']);
        return of(false);
      })

    )
  }
}
      // (response : any) => {
      //   console.log("============response=============",response);
      //   this.commonService.maintainSession(response);
      //   this.commonService.showLoader = false;
      //   this.commonService.isLoggedIn = true;
      // },
      // (error : any) => {
      //   this.commonService.error(error);
      //   this.commonService.showLoader = false;
      // }