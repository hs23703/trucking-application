import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  showLoader: boolean = false;
  snackBar: any;
  isLoggedIn: boolean = false;

  constructor(public _snackBar: MatSnackBar) { }

  error(error: any) {
    console.log("=============error============", error);
    if (error.error && error.error.message)
      this.showSnackBar(error.error.message);
    else
      this.showSnackBar("some error occured")

  }
  showSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 2000
    });
  }
  maintainSession(obj: any) {
    window.localStorage.setItem("login", JSON.stringify(obj));
  }
  endSession() {
    window.localStorage.removeItem('login');
  }

}
