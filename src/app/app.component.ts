import { Component, OnInit } from '@angular/core';
import { CommonService } from './services/common.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'TruckingApp';
  constructor( public commonService:CommonService){

  }
  ngOnInit(){
  }
}
