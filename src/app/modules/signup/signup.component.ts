import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { SignupService } from './signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
 public signupForm : any;

  constructor(private fb : FormBuilder, private signupService : SignupService, 
    private commonService : CommonService, private router : Router) { }

  ngOnInit(): void {
    this.formInit();
  }
  formInit() {
    this.signupForm = this.fb.group(
      {
        name : ['',Validators.required],
        email: ['',[Validators.required, Validators.email]],
        password: ['',Validators.required],
        phone_no: ['',[Validators.required, Validators.maxLength(10), Validators.minLength(10)]],
      }
    )
  }
  submit(){
    console.log("====================signup Form=============",this.signupForm);
    if(this.signupForm.status == "INVALID")
    return;

    this.commonService.showLoader = true;
    console.log("=================form data=============",this.signupForm.value);
    this.signupService.registerNew(this.signupForm.value).subscribe(
      (response : any) => {
        console.log("============response=============",response);
        this.commonService.maintainSession(response);
        this.commonService.showLoader = false;
        this.commonService.isLoggedIn = true;
        this.router.navigate(['/home'])
      },
      (error : any) => {
        this.commonService.error(error);
        this.commonService.showLoader = false;
      }
    )
  }

}
