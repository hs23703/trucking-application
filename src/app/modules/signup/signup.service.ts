import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor( private apiService : ApiService) { }

  registerNew(data: any)
  {
    const url = "https://api.backendless.com/DC44146C-18A7-1455-FFEF-12B0504ABB00/5EF2E84C-38CB-4138-982B-CFE451A3718A/users/register";
    return this.apiService.post(url,data);
  }
}
