import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private fb : FormBuilder, private loginService : LoginService,
     private commonService : CommonService, private router: Router) { }
  public loginForm : any;

  ngOnInit(): void {
    this.formInit();
  }
  formInit() {
    this.loginForm = this.fb.group(
      {
        login: ['',[Validators.required, Validators.email]],
        password: ['',Validators.required],
      }
    )
  }
  submit(){
    if(this.loginForm.status == "INVALID")
    return;

    this.commonService.showLoader = true;
    console.log("=================form data=============",this.loginForm.value);
    this.loginService.login(this.loginForm.value).subscribe(
      (response : any) => {
        console.log("============response=============",response);
        this.commonService.maintainSession(response);
        this.commonService.showLoader = false;
        this.commonService.isLoggedIn = true;
        this.router.navigate(['/home'])
      },
      (error : any) => {
        this.commonService.error(error);
        this.commonService.showLoader = false;
      }
    )
  }

}
