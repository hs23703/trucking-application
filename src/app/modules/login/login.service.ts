import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private apiService : ApiService) {}
  
  login(data: any)
  {
    const url = "https://api.backendless.com/DC44146C-18A7-1455-FFEF-12B0504ABB00/5EF2E84C-38CB-4138-982B-CFE451A3718A/users/login";
    return this.apiService.post(url,data);
  }
  loginViaUserToken(userToken:string){
       const url = "https://api.backendless.com/DC44146C-18A7-1455-FFEF-12B0504ABB00/5EF2E84C-38CB-4138-982B-CFE451A3718A/users/isvalidusertoken/" + userToken ;
      return this.apiService.get(url);
  }

}
