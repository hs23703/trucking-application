import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { NewBookingService } from './new-booking.service';

@Component({
  selector: 'app-new-booking',
  templateUrl: './new-booking.component.html',
  styleUrls: ['./new-booking.component.scss']
})
export class NewBookingComponent implements OnInit {
  searchForm: any;
  options: any;


  constructor(private fb : FormBuilder, private newBookingService : NewBookingService, private commonService : CommonService, private router : Router) { }

  ngOnInit(): void {
    this.initForm();
  }
  initForm() {
    this.searchForm = this.fb.group(
      {
        origin: ['',Validators.required],
        destination: ['',Validators.required],
        capacity : ['',Validators.required]
      }
    )
  }
  
  search()
  {
    if(this.searchForm.status == "INVALID")
    return;
    this.newBookingService.getOptions('').subscribe(
      (response) => {
        console.log("============response=============",response);
        this.options = response;
      }
    )
  }
  book(){
    this.commonService.showSnackBar("Booking Done");
    this.router.navigate(["/home"])
  }


}
