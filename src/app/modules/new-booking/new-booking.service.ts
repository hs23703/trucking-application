import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewBookingService {

  constructor() { }

  availableOptions = [
    {
      plateNo : "PB02 CX 2238",
      type : "Heavy",
      price : "Rs. 2000",
      time : "22 hrs",
    },
    {
      plateNo : "PB02 CX 2238",
      type : "Semi Heavy",
      price : "Rs. 2500",
      time : "19 hrs",
    },
    {
      plateNo : "PB02 CX 2238",
      type : "Medium",
      price : "Rs. 1500",
      time : "25 hrs",
    },
    {
      plateNo : "PB02 CX 2238",
      type : "Light",
      price : "Rs. 1700",
      time : "25 hrs",
    },
    {
      plateNo : "PB02 CX 2238",
      type : "Heavy",
      price : "Rs. 2600",
      time : "17 hrs",
    },
    {
      plateNo : "PB02 CX 2238",
      type : "Light",
      price : "Rs. 2000",
      time : "22 hrs",
    }
  ]

  getOptions( obj : any ){
    return of(this.availableOptions);
  }
}
