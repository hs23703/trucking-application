import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor( private commonService : CommonService, private router : Router) { }

  ngOnInit(): void {
  }

  logout(){
    this.commonService.showSnackBar("Logout Successful");
    this.commonService.isLoggedIn = false;
    this.commonService.endSession();
    this.router.navigate(['/login']);
  }
  goToHome(){
    this.router.navigate(['/home'])
  }

}
