import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HeaderModule } from '../header/header.module';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';

const routes = [ 
  {path : '', component: HomeComponent},
  ];
  


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HeaderModule,
    MatButtonModule
  ]
})
export class HomeModule { }
