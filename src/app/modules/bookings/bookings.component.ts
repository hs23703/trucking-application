import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { BookingsService } from './bookings.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss']
})
export class BookingsComponent implements OnInit {
  bookings: any;

  constructor( private bookingsService : BookingsService, private commonService : CommonService) { }

  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this.bookingsService.getData().subscribe(
      (response) => {
        console.log("========response=============",response);
        this.bookings = response;
      }
    )
  }
  cancel(i:number){
    this.bookings.splice(i,1);
    this.commonService.showSnackBar("Cancelled");
  }
  track(i:number) {
    window.open(this.bookings[i].trackingLink,"_blank");
  }

}
