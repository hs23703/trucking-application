import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookingsService {

  constructor() { }
  bookingsData =[
    { 
      id : "3433434388",
      name : "truck1",
      type : "Heavy",
      origin : "address 1",
      destination : "address 2",
      currentLocation : "location 1",
      bookingStatus : "Pending",
      trackingLink : "https://www.google.com/maps/place/Delhi/@28.6795135,76.9497154,11z/data=!4m5!3m4!1s0x390cfd5b347eb62d:0x37205b715389640!8m2!3d28.7040592!4d77.1024902",
      tod: "date1",
      expectedToa: "date2",
    },
    { 
      id : "3433434388",
      name : "truck1",
      type : "Heavy",
      origin : "address 1",
      destination : "address 2",
      currentLocation : "location 1",
      bookingStatus : "Pending",
      tod: "date1",
      expectedToa: "date2",
    },
    { 
      id : "3433434388",
      name : "truck1",
      type : "Heavy",
      origin : "address 1",
      destination : "address 2",
      currentLocation : "location 1",
      bookingStatus : "Transit",
      trackingLink : "https://www.google.com/maps/place/Delhi/@28.6795135,76.9497154,11z/data=!4m5!3m4!1s0x390cfd5b347eb62d:0x37205b715389640!8m2!3d28.7040592!4d77.1024902",
      tod: "date1",
      expectedToa: "date2",
    },
    { 
      id : "3433434388",
      name : "truck1",
      type : "Heavy",
      origin : "address 1",
      destination : "address 2",
      currentLocation : "location 1",
      bookingStatus : "Pending",
      tod: "date1",
      expectedToa: "date2",
    },
    { 
      id : "3433434388",
      name : "truck1",
      type : "Heavy",
      origin : "address 1",
      destination : "address 2",
      currentLocation : "location 1",
      bookingStatus : "Transit",
      trackingLink : "https://www.google.com/maps/place/Delhi/@28.6795135,76.9497154,11z/data=!4m5!3m4!1s0x390cfd5b347eb62d:0x37205b715389640!8m2!3d28.7040592!4d77.1024902",
      tod: "date1",
      expectedToa: "date2",
    }
  ]

  getData() {
    return of(this.bookingsData);
  }
}
